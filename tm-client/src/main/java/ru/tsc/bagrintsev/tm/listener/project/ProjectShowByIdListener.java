package ru.tsc.bagrintsev.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectShowByIdRequest;
import ru.tsc.bagrintsev.tm.dto.response.project.ProjectShowByIdResponse;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectShowByIdListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(id);
        @Nullable final ProjectShowByIdResponse response = projectEndpoint.showProjectById(request);
        @Nullable final ProjectDto project = response.getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String name() {
        return "project-show";
    }

}
