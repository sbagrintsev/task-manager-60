package ru.tsc.bagrintsev.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignUpRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public final class UserSignUpListener extends AbstractUserListener {

    @NotNull
    @Override
    public String description() {
        return "Sign-up new user in system.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userSignUpListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo(EntityField.PASSWORD);
        @NotNull final String password = TerminalUtil.nextLine();
        showParameterInfo(EntityField.EMAIL);
        @NotNull final String email = TerminalUtil.nextLine();
        userEndpoint.signUp(new UserSignUpRequest(login, password, email));
    }

    @NotNull
    @Override
    public String name() {
        return "sign-up";
    }

}
