package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataBase64SaveRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataBase64SaveListener extends AbstractDataListener {

    @NotNull
    @Override
    public String description() {
        return "Save current application state in base64 file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64SaveListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.saveBase64(new DataBase64SaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-base64";
    }

}
