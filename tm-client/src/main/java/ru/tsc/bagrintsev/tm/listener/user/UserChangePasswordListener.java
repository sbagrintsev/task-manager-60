package ru.tsc.bagrintsev.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.user.UserChangePasswordRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull
    @Override
    public String description() {
        return "Change user password.";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userChangePasswordListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        System.out.printf("Enter old password: %n");
        @NotNull final String oldPassword = TerminalUtil.nextLine();
        System.out.printf("Enter new password: %n");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setNewPassword(newPassword);
        request.setOldPassword(oldPassword);
        userEndpoint.changePassword(request);
    }

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

}
