package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskBindToProjectRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class TaskBindToProjectListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "Bind task to project.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskBindToProjectListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.PROJECT_ID);
        @Nullable final String projectId = TerminalUtil.nextLine();
        showParameterInfo(EntityField.TASK_ID);
        @Nullable final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpoint.bindTaskToProject(request);
    }

    @NotNull
    @Override
    public String name() {
        return "task-bind";
    }

}
