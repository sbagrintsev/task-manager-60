package ru.tsc.bagrintsev.tm.enumerated;

public enum EventType {

    INSERT,
    DELETE,
    UPDATE,

}
