package ru.tsc.bagrintsev.tm.model;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "m_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractWBSModel {

    @Nullable
    @ManyToOne
    private Project project;

}
