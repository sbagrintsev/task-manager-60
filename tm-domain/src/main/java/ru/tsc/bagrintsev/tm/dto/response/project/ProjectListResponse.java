package ru.tsc.bagrintsev.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractProjectResponse {

    @Nullable
    private List<ProjectDto> projects;

    public ProjectListResponse(@Nullable final List<ProjectDto> projects) {
        this.projects = projects;
    }

}
