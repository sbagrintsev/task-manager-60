package ru.tsc.bagrintsev.tm.dto.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedDtoModel extends AbstractDtoModel {

    @Nullable
    @Column(name = "user_id")
    private String userId;

}
