package ru.tsc.bagrintsev.tm.dto.model;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractDtoModel implements Serializable {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

}
