package ru.tsc.bagrintsev.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;

@Repository
@Scope("prototype")
public class ProjectDtoRepository extends UserOwnedDtoRepository<ProjectDto> implements IProjectDtoRepository {

    public ProjectDtoRepository() {
        super(ProjectDto.class);
    }

}
