package ru.tsc.bagrintsev.tm.repository.dto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.api.repository.dto.IAbstractDtoRepository;
import ru.tsc.bagrintsev.tm.dto.model.AbstractDtoModel;

import java.util.Collection;
import java.util.List;

@Repository
@Scope("prototype")
@RequiredArgsConstructor
public class AbstractDtoRepository<M extends AbstractDtoModel> implements IAbstractDtoRepository<M> {

    @NotNull
    protected final Class<M> clazz;

    @Nullable
    @PersistenceContext
    protected EntityManager entityManager;


    public void add(@NotNull final M record) {
        entityManager.persist(record);
    }

    @Override
    public void addAll(@NotNull final Collection<M> records) {
        records.forEach(this::add);
    }

    @Override
    public void clearAll() {
        @NotNull final String jpql = String.format("DELETE FROM %s", clazz.getSimpleName());
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public @Nullable List<M> findAll() {
        @NotNull final String jpql = String.format("FROM %s", clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .getResultList();
    }

    @Override
    public @Nullable M findOneById(@NotNull String id) {
        return entityManager.find(clazz, id);
    }

    @Override
    public long totalCount() {
        @NotNull final String jpql = String.format("SELECT count(*) FROM %s", clazz.getSimpleName());
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

}
