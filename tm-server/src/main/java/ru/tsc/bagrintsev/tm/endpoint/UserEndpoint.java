package ru.tsc.bagrintsev.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IAuthDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserDtoService;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;
import ru.tsc.bagrintsev.tm.dto.request.user.*;
import ru.tsc.bagrintsev.tm.dto.response.user.*;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;

@Controller
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final IUserDtoService userService;

    public UserEndpoint(
            @NotNull final IUserDtoService userService,
            @NotNull final IAuthDtoService authService,
            @NotNull final IUserDtoService userService1
    ) {
        super(userService, authService);
        this.userService = userService1;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String newPassword = request.getNewPassword();
        @Nullable final String oldPassword = request.getOldPassword();
        @NotNull final UserDto user;
        try {
            user = userService.setPassword(userId, newPassword, oldPassword);
        } catch (IdIsEmptyException e) {
            throw new UserNotFoundException();
        }
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLockResponse lock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        userService.lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserRemoveResponse remove(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        userService.removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserSetRoleResponse setRole(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserSetRoleRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final Role role = request.getRole();
        @NotNull final UserDto user = userService.setRole(login, role);
        return new UserSetRoleResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserSignUpResponse signUp(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserSignUpRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final UserDto user = userService.create(login, password);
        userService.setParameter(user, EntityField.EMAIL, email);
        return new UserSignUpResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserUnlockResponse unlock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        userService.unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserUpdateProfileResponse updateProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String lastName = request.getLastName();
        @NotNull final UserDto user;
        try {
            user = userService.updateUser(userId, firstName, lastName, middleName);
        } catch (IdIsEmptyException e) {
            throw new UserNotFoundException();
        }
        return new UserUpdateProfileResponse(user);
    }

}
