package ru.tsc.bagrintsev.tm.repository.model;

import javax.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public class TaskRepository extends WBSRepository<Task> implements ITaskRepository {

    public TaskRepository() {
        super(Task.class);
    }

    @Override
    public @Nullable List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.user.id = :userId AND m.project.id = :projectId",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final Project project
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.project = :project " +
                        "WHERE m.user.id = :userId " +
                        "AND m.id = :id",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("project", project)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .executeUpdate();
    }

}
