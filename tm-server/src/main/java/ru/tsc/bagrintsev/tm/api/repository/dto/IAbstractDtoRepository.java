package ru.tsc.bagrintsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.AbstractDtoModel;

import java.util.Collection;
import java.util.List;

public interface IAbstractDtoRepository<M extends AbstractDtoModel> {

    void add(@NotNull final M record);

    void addAll(@NotNull final Collection<M> records);

    void clearAll();

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull final String id);

    long totalCount();

}
