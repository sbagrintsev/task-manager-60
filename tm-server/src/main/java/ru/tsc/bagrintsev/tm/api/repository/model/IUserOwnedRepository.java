package ru.tsc.bagrintsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    void clear(@NotNull final String userId);

    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Nullable
    List<M> findAllByUserId(@NotNull final String userId);

    @Nullable
    M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

    long totalCountByUserId(@NotNull final String userId);

    void update(@NotNull final M record);

}
