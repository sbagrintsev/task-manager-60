package ru.tsc.bagrintsev.tm.service.dto;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.bagrintsev.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectTaskDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskDtoService;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    private final IProjectDtoService projectService;

    @NotNull
    private final ITaskDtoService taskService;

    @NotNull
    private final IProjectDtoRepository projectRepository;

    @NotNull
    private final ITaskDtoRepository taskRepository;

    @Override
    @Transactional
    public TaskDto bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws IdIsEmptyException, ProjectNotFoundException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        if (taskId == null || taskId.isEmpty()) throw new IdIsEmptyException(EntityField.TASK_ID.getDisplayName());
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskService.existsById(userId, taskId)) throw new TaskNotFoundException();
        return taskService.setProjectId(userId, taskId, projectId);
    }

    @Override
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        @Nullable final List<TaskDto> tasks = taskRepository.findAllByProjectId(userId, projectId);
        if (tasks != null) tasks.forEach((t) -> taskRepository.removeById(userId, t.getId()));
        @Nullable ProjectDto project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.removeById(userId, projectId);
    }

    @Override
    @Transactional
    public TaskDto unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (taskId == null || taskId.isEmpty()) throw new IdIsEmptyException(EntityField.TASK_ID.getDisplayName());
        return taskService.setProjectId(userId, taskId, null);
    }

}
