package ru.tsc.bagrintsev.tm.service.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.bagrintsev.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.bagrintsev.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserDtoService;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.EmailIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.exception.system.EmptyArgumentException;
import ru.tsc.bagrintsev.tm.exception.user.*;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserDtoService extends AbstractDtoService<UserDto, IUserDtoRepository> implements IUserDtoService {

    @Getter
    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ITaskDtoRepository taskRepository;

    @NotNull
    private final IProjectDtoRepository projectRepository;

    @NotNull
    private final IUserDtoRepository userRepository;

    @NotNull
    @Override
    public UserDto checkUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws PasswordIsIncorrectException, AccessDeniedException, LoginIsIncorrectException, GeneralSecurityException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (password == null || password.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final UserDto user = findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        if (!(HashUtil.generateHash(password, user.getPasswordSalt(), iterations, keyLength)).equals(user.getPasswordHash())) {
            throw new PasswordIsIncorrectException();
        }
        return user;
    }

    @Override
    @Transactional
    public void clearAll() {
        userRepository.clearAll();
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password
    ) throws GeneralSecurityException, PasswordIsIncorrectException, LoginIsIncorrectException, LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (password == null || password.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(password, salt, iterations, keyLength);
        @NotNull UserDto user = new UserDto(login, passwordHash, salt);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public List<UserDto> findAll() {
        @Nullable final List<UserDto> result = userRepository.findAll();
        return (result == null) ? Collections.emptyList() : result;
    }

    @NotNull
    @Override
    public UserDto findByEmail(@Nullable final String email) throws EmailIsEmptyException, UserNotFoundException {
        if (email == null || email.isEmpty()) throw new EmailIsEmptyException();
        @Nullable UserDto user = userRepository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public UserDto findByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable UserDto user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public @NotNull UserDto findOneById(@Nullable String id) throws IdIsEmptyException, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable UserDto user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExists(email);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isLoginExists(login);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable final UserDto user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        userRepository.setParameter(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDto removeByLogin(@Nullable final String login) throws LoginIsIncorrectException, IdIsEmptyException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable UserDto user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        userRepository.removeByLogin(login);
        return user;
    }

    @Override
    @Transactional
    public @NotNull Collection<UserDto> set(@NotNull Collection<UserDto> records) {
        if (records.isEmpty()) return records;
        userRepository.addAll(records);
        return records;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto setParameter(
            @Nullable final UserDto user,
            @NotNull final EntityField paramName,
            @Nullable final String paramValue
    ) throws EmailAlreadyExistsException, UserNotFoundException, IncorrectParameterNameException, EmptyArgumentException {
        if (user == null) throw new UserNotFoundException();
        if (paramValue == null) throw new EmptyArgumentException();
        if (EntityField.EMAIL.equals(paramName) && isEmailExists(paramValue)) {
            throw new EmailAlreadyExistsException(paramValue);
        }
        @Nullable UserDto userResult;
        switch (paramName) {
            case EMAIL:
                user.setEmail(paramValue);
                break;
            case FIRST_NAME:
                user.setFirstName(paramValue);
                break;
            case MIDDLE_NAME:
                user.setMiddleName(paramValue);
                break;
            case LAST_NAME:
                user.setLastName(paramValue);
                break;
            case LOCKED:
                user.setLocked("true".equals(paramValue));
                break;
            default:
                throw new IncorrectParameterNameException(paramName, "User");
        }
        userRepository.setParameter(user);
        userResult = userRepository.findOneById(user.getId());
        if (userResult == null) throw new UserNotFoundException();
        return userResult;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto setPassword(
            @Nullable final String userId,
            @Nullable final String newPassword,
            @Nullable final String oldPassword
    ) throws GeneralSecurityException, PasswordIsIncorrectException, IdIsEmptyException, AccessDeniedException, LoginIsIncorrectException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (newPassword == null || newPassword.isEmpty()) throw new PasswordIsIncorrectException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(newPassword, salt, iterations, keyLength);
        @Nullable UserDto user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        checkUser(user.getLogin(), oldPassword);
        userRepository.setUserPassword(user.getLogin(), passwordHash, salt);
        user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto setRole(
            @Nullable final String login,
            @Nullable final Role role
    ) throws IncorrectRoleException, LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (role == null) throw new IncorrectRoleException();
        userRepository.setRole(login, role);
        @Nullable UserDto user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public long totalCount() {
        return userRepository.totalCount();
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable final UserDto user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        userRepository.setParameter(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDto updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws IdIsEmptyException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @Nullable UserDto user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        userRepository.setParameter(user);
        return user;
    }

}
