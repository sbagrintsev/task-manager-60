package ru.tsc.bagrintsev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;

import java.util.List;

@Repository
@Scope("prototype")
public class TaskDtoRepository extends UserOwnedDtoRepository<TaskDto> implements ITaskDtoRepository {

    public TaskDtoRepository() {
        super(TaskDto.class);
    }

    @Override
    public @Nullable List<TaskDto> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.userId = :userId AND m.projectId = :projectId",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.projectId = :projectId " +
                        "WHERE m.userId = :userId " +
                        "AND m.id = :id",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .executeUpdate();
    }

}
