package ru.tsc.bagrintsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;

import java.util.Collection;
import java.util.List;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {

    @Override
    void add(@NotNull final ProjectDto record);

    @Override
    void addAll(@NotNull final Collection<ProjectDto> records);

    @Override
    void clear(@NotNull final String userId);

    @Override
    void clearAll();

    @Override
    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    @Nullable
    List<ProjectDto> findAll();

    @Override
    @Nullable
    List<ProjectDto> findAllByUserId(@NotNull final String userId);

    @Override
    @Nullable
    List<ProjectDto> findAllSort(
            @NotNull final String userId,
            @NotNull final String order
    );

    @Override
    @Nullable
    ProjectDto findOneById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    long totalCount();

    @Override
    long totalCountByUserId(@NotNull final String userId);

    @Override
    void update(@NotNull final ProjectDto project);

    @Override
    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
