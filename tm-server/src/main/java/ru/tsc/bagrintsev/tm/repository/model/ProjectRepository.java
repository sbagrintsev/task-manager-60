package ru.tsc.bagrintsev.tm.repository.model;

import javax.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.bagrintsev.tm.model.Project;

@Repository
@Scope("prototype")
public class ProjectRepository extends WBSRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
        super(Project.class);
    }

}
