package ru.tsc.bagrintsev.tm.endpoint;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IAuthDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserDtoService;
import ru.tsc.bagrintsev.tm.dto.model.SessionDto;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.PermissionDeniedException;

import java.util.Arrays;

@Controller
@RequiredArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    protected final IUserDtoService userService;

    @NotNull
    protected final IAuthDtoService authService;

    @SneakyThrows
    protected SessionDto check(
            @NotNull final AbstractUserRequest request,
            @NotNull final Role role
    ) {
        return check(request, new Role[]{role});
    }

    @SneakyThrows
    protected SessionDto check(
            @NotNull final AbstractUserRequest request,
            @NotNull final Role[] roles
    ) {
        @Nullable String token = request.getToken();
        @NotNull final SessionDto session = authService.validateToken(token);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new AccessDeniedException();
        @NotNull final UserDto user = userService.findOneById(userId);
        @NotNull final Role userRole = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(userRole);
        if (!hasRole) throw new PermissionDeniedException();
        return session;
    }

}
